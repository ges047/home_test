-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: bbe
Binary: bbe
Architecture: any
Version: 0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1
Maintainer: Debian QA Group <packages@qa.debian.org>
Homepage: http://sourceforge.net/projects/bbe-/
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/debian/bbe
Vcs-Git: https://salsa.debian.org/debian/bbe.git
Build-Depends: debhelper-compat (= 13), texinfo
Package-List:
 bbe deb editors optional arch=any
Checksums-Sha1:
 42d5b47d607a9633fb49e7d39e2aebfb7bb89c05 211590 bbe_0.2.2.orig.tar.gz
 497a7edd61e7f1e400f99f96f419cb3c834fd87f 9060 bbe_0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1.debian.tar.xz
Checksums-Sha256:
 baaeaf5775a6d9bceb594ea100c8f45a677a0a7d07529fa573ba0842226edddb 211590 bbe_0.2.2.orig.tar.gz
 4e9f8568730121fb2188c484b65e8cc1495936371e286fecd2eacb84783c6f04 9060 bbe_0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1.debian.tar.xz
Files:
 b056d0bfd852384aced73d4533887d4b 211590 bbe_0.2.2.orig.tar.gz
 204ef6a8847770a26c76bd9120b22ecd 9060 bbe_0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHFBAEBCgAvFiEEm/nJivBNmd8h2TfhpMYho4lN1yMFAmKMBpQRHGdlczA0N0Bn
bWFpbC5jb20ACgkQpMYho4lN1yM7Tgv/QeooO3b4ERZv2qCY5b5m9JXpgSBIFSuN
fTNh/F5mZbXwh1n9Smrlwp5Fe5sY5UXPeMOvPAJXK4SoewhQcxqG7EcpKtJsOlfF
wzNQ13cAnMLV2YBI/92g+F/aZ63vfayusXeZAj3JIPErL0LxEMJP3Bvl6VYwHbJG
FkCHHS41KTGOTQl1uogBwrbmEtKauM2ATaOINXAy+z72p8P+DHucqkCbivKsdqQw
n00iM7Elzwih58FWpiWuXa2CJT4cc7rt472qH+MQhGkn2F66XjPzuwLFGrrtKtD0
TDTdmHg8A2hPDPQuMJW58unaSodhuoG2hsd0DXGGi0VsvcMuUILrF+NoDr5d0lf9
Rc9Q4XfTJmnHUakOiAOTG12Qc9vqwWMQx2bWhtXSCTEmiKYNv5fQDbEameotx8nj
f+3hK7gfqOwsTHpuc/rvVF0Y4Jq1lqWbCeR6P+TDZEy7lycj/jEPhMD8VgAoMCHi
WfxKGMf983HmV/pRp12z7+YIgCcPQFnw
=TK2i
-----END PGP SIGNATURE-----
