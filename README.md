# home_test

## Getting started
This project is based on bbe debain package to do installation testing.

## Build debian package
```
$ cd bbe-0.2.2
bbe-0.2.2$ dpkg-buildpackage
...
bbe-0.2.2$ cd ..
$
```
## Install bbe debian package
```
$ sudo dpkg -i bbe_0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1_amd64.deb
Selecting previously unselected package bbe.
(Reading database ... 221842 files and directories currently installed.)
Preparing to unpack bbe_0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1_amd64.deb ...
Unpacking bbe (0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1) ...
Setting up bbe (0.2.2-4+3SNAPSHOT20220524ubuntu1ppa1) ...
this is a test from Brent Lin
Processing triggers for install-info (6.8-4build1) ...
Processing triggers for man-db (2.10.2-1) ...
$
```
## Past executing result
```
$ dpkg -S /usr/bin/testing.sh ; /usr/bin/testing.sh 
bbe: /usr/bin/testing.sh
this is a test from Brent Lin
$
```
## Remove bbe
```
$ sudo apt-get remove bbe
$
```
